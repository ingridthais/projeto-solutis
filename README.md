# Robô para Robot Arena Solutis 2020

Olá, meu nome é Ingrid Thais, tenho 26 anos, sou estudante de Análise e Desenvolvimento de Sistemas e uma entusiasta em programação! 

Então, o objetivo principal deste robô é desviar dos inimigos, assim podendo atirar neles, enquanto foge de suas balas.

# Detalhando o código:

- No inicio do código, define as cores de todos os equipamentos do meu robô.

- Logo abaixo definir alguns movimentos para que ele se mova rapidamente e gire 90 graus a direita e 180 a esquerda e depois que ele virar a esquerda, vira a direita novamente a 180 graus.

- Quando o Jarvis scanear um robo (onScannedRobot) ele atira nele e continua se esquivando.

- Quando o Jarvis bater em um robo (onHitRobot) ele atira nele até conseguir sair.

- Quando o Jarvis bater na parede (onHitWall) reverte a posição rapidamente e continua a frente.

- A principal vantagem é que ele consegue se esquivar rapidamente dos inimigos e tenta acerta-lós sem se ferir, ele tenta ao máximo se esquivar das balas inimigas e atingir o inimigo até destrui-ló. A desvantagem é que o dano dele é baixo, porque ele não consegue fazer uma sequencia de hits, pois ele não freia para poder atirar nos outros robôs.

- O que eu mais gostei de ter feito nesse projeto é que a gente pode se divertir programando, aprendi muitas coisas para deixar meu robozinho melhor dos que os outros, então tive que pesquisar bastante, para deixa-ló o mais poderoso possivel.
